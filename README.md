# Eatwell



## 100 Days Of Code - 2024 Web Development Bootcamp

This project is from [100 Days Of Code - 2024 Web Development Bootcamp](https://www.udemy.com/course/100-days-of-code-web-development-bootcamp/) course 
by Academind.

### Section 19: More Express: Static & Dynamic Content with Templates (EJS) [Days 49 - 51]

The project goal is dive deep into backend development with Node.js and learn new things about it.