const express = require('express');

const uuid = require('uuid');

const { getStoredRestaurants, storeRestaurants } = require('../util/restaurant-data');

const router = express.Router();

router.get('/restaurants', (req, res) => {
  const order = req.query.order || 'asc';
  const storedRestaurants = getStoredRestaurants();

  storedRestaurants.sort((a, b) => {
    if ((order === 'asc' && a.name > b.name) || (order === 'desc' && b.name > a.name)) {
      return 1;
    }

    return -1;
  });

  res.render('restaurants', {
    numberOfRestaurants: storedRestaurants.length,
    restaurants: storedRestaurants,
    order: order === 'asc' ? 'desc' : 'asc',
  });
});

router.get('/restaurants/:id', (req, res) => {
  const restaurantId = req.params.id;

  const storedRestaurants = getStoredRestaurants();
  const restaurant = storedRestaurants.find(restaurant => restaurant.id === restaurantId);
  if (restaurant) res.render('restaurant-detail', { restaurant });
  res.status(404).render('404');
});

router.get('/recommend', (req, res) => {
  res.render('recommend');
});

router.post('/recommend', (req, res) => {
  const restaurant = req.body;
  restaurant.id = uuid.v4();

  const storedRestaurants = getStoredRestaurants();
  storedRestaurants.push(restaurant);
  storeRestaurants(storedRestaurants);

  res.redirect('/confirm');
});

router.get('/confirm', (req, res) => {
  res.render('confirm');
});

module.exports = router;
